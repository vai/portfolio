---
date: 2017-02-25T17:53:13+02:00
description: "Now the beast is unleashed!"
draft: false
featured: false
image: /images/illustration/platyblanka.jpg
share: false
slug: platyblanka
tags:
- digital
- comic art
- platymoose
- street fighter
title: Platyfight Blanka
---
![Platyfight blanka](/images/illustration/platyblanka.jpg)
Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose.

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

"Whroo-oooooooooooooooo!"