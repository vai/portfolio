---
date: 2017-02-28T22:34:05+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-snarl.jpg
share: false
slug: mugshot-snarl
tags:
- digital
- portraiture
- mugshot
title: Snarl
---
![Snarl](/images/illustration/mugshot-snarl.jpg "snarl for the cameras")

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3
