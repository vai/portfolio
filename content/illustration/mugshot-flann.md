---
date: 2017-02-28T22:34:09+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-flann.jpg
share: false
slug: mugshot-flann
tags:
- digital
- portraiture
- mugshot
- retro
title: My name is Flann
---
![Flann](/images/illustration/mugshot-flann.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3