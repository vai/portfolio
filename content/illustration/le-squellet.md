---
date: 2017-02-25T18:06:57+02:00
description: "the strong bits"
draft: false
featured: false
image: /images/illustration/le-squellet.jpg
share: false
slug: le-squellet
tags:
- digital
- vector
- educational
- french
- adventure time
title: Le Squellet
---
![The Skeleton](/images/illustration/le-squellet.jpg "the skeleton")

*Medium:* Digital (vector), Clip Studio Paint, Wacom Intuos3

Educational poster to learn the body parts en français