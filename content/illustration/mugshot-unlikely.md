---
date: 2017-02-28T22:34:16+02:00
description: ""
draft: false
featured: true
image: /images/illustration/mugshot-unlikely.jpg
share: false
slug: mugshot-unlikely
tags:
- digital
- portraiture
- mugshot
- retro
title: Unlikely suspect
---
![Unlikely Suspect](/images/illustration/mugshot-unlikely.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3
