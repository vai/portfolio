---
date: 2017-02-25T18:00:45+02:00
description: "mathematical"
draft: false
featured: false
image: /images/illustration/platyfinn.jpg
share: false
slug: platyfinn
tags:
- digital
- comic art
- platymoose
- adventure time
- street fighter
title: Platyfight Finn
---
![Platyfight Finn](/images/illustration/platyfinn.jpg "mathematical!")
Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose.

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

Cross over with Adventure Time !