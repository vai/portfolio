---
date: 2017-02-28T22:34:14+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-squish.jpg
share: false
slug: mugshot-squish
tags:
- digital
- portraiture
- mugshot
- retro
title: Squish
---
![Squish](/images/illustration/mugshot-squish.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3