---
date: 2017-02-28T22:34:15+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-serious.jpg
share: false
slug: mugshot-serious
tags:
- digital
- portraiture
- mugshot
title: It's serious
---
![It's serious](/images/illustration/mugshot-serious.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3