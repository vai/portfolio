---
date: 2017-02-28T22:34:17+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-cateyes.jpg
share: false
slug: mugshot-cateyes
tags:
- digital
- portraiture
- mugshot
- retro
title: Cat Eyes
---
![Cat eyes](/images/illustration/mugshot-cateyes.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3