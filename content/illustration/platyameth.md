---
date: 2017-03-18T18:26:51+02:00
description: ""
draft: false
featured: false
image: /images/illustration/platyameth-crop.jpg
share: false
slug: platyameth
tags:
- digital
- comic art
- platymoose
- street fighter
- steven universe
title: PlatyAmethyst
---

![PlatyAmethyst](/images/illustration/platyameth.jpg)

**Medium:** Digital, Clip Studio Paint, Wacom MobileStudio Pro