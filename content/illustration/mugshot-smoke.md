---
date: 2017-02-28T22:34:13+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-smoke.jpg
share: false
slug: mugshot-smoke
tags:
- digital
- portraiture
- mugshot
- retro
title: Smoke
---
![Smoke](/images/illustration/mugshot-smoke.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3