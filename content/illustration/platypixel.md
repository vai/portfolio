---
date: 2017-02-24T20:20:34+02:00
description: "* pew pew *"
draft: false
featured: false
image: "/images/illustration/platypixel.jpg"
share: false
slug: platypixel
tags:
- digital
- comic art
- platymoose
- retro
title: Platypixel
---
![Platypixel](/images/illustration/platypixel.jpg "pew pew")
Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose. This time retro pixel style

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3


