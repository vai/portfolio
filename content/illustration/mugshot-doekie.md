---
date: 2017-02-28T22:34:06+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-doekie.jpg
share: false
slug: mugshot-dont-mess
tags:
- digital
- portraiture
- mugshot
- retro
title: Don't Mess
---
![Don't Mess](/images/illustration/mugshot-doekie.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3