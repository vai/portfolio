---
date: 2017-02-28T22:34:11+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-frankie-side.jpg
share: false
slug: mugshot-frankie-side
tags:
- digital
- portraiture
- mugshot
title: Frankie Stein - Side
---
![Frankie Stein](/images/illustration/mugshot-frankie-side.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3