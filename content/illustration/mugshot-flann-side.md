---
date: 2017-02-28T22:34:08+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-flann-side.jpg
share: false
slug: mugshot-flann-side
tags:
- digital
- portraiture
- mugshot
title: My name is Flann - Side
---
![Flann - Side](/images/illustration/mugshot-flann-side.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3