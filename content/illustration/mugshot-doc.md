---
date: 2017-02-28T22:34:07+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-doc.jpg
share: false
slug: mugshot-doc
tags:
- digital
- portraiture
- mugshot
- retro
title: Doc
---
![Doc](/images/illustration/mugshot-doc.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3