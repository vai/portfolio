---
date: 2017-02-25T18:06:59+02:00
description: "The fleshy bits"
draft: false
featured: false
image: /images/illustration/le-corps.jpg
share: false
slug: le-corps
tags:
- digital
- vector
- educational
- french
- adventure time
title: Le Corps
---
![The Body](/images/illustration/le-corps.jpg "the body")

*Medium:* Digital (vector), Clip Studio Paint, Wacom Intuos3

Educational poster to learn the body parts en français
