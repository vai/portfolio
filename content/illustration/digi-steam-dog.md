---
date: 2017-03-03T19:04:29+02:00
description: ""
draft: false
featured: false
image: /images/illustration/digi-steam-dog.jpg
share: false
slug: steam-dog
tags:
- digital
- portraiture
- animals
- steampunk
title: Steampunk Doge
---

![Steampunk Doge](/images/illustration/digi-steam-dog.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3

