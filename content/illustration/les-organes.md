---
date: 2017-02-25T18:06:58+02:00
description: "The squishy bits"
draft: false
featured: false
image: /images/les-organes.jpg
share: false
slug: les-organes
tags:
- digital
- vector
- educational
- french
- adventure time
title: Les Organes
---
![The Organs](/images/les-organes.jpg "the organs")

*Medium:* Digital (vector), Clip Studio Paint, Wacom Intuos3

Educational poster to learn the body parts en français