---
date: 2017-02-28T22:34:12+02:00
description: ""
draft: false
featured: false
image: /images/illustration/mugshot-frankie.jpg
share: false
slug: mugshot-frankie
tags:
- digital
- portraiture
- mugshot
title: Frankie Stein
---
![Frankie Stein](/images/illustration/mugshot-frankie.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3