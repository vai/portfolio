---
date: 2017-03-03T11:19:45+02:00
description: ""
draft: false
featured: false
image: /images/illustration/chibi-girl.jpg
share: false
slug: chibi-girl
tags:
- digital
- comic art
- chibi
- gift

title: Chibi Girl
---

![Chibi Girl](/images/illustration/chibi-girl.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3

