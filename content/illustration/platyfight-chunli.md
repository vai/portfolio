---
date: 2017-03-14T01:46:46+02:00
description: ""
draft: false
featured: false
image: /images/illustration/platyfight-chunli-crop.jpg
share: false
slug: platyfight-chunli
tags:
- digital
- comic art
- platymoose
- street fighter
title: Chunli
---

![Chunli](/images/illustration/platyfight-chunli.jpg)

Part of my PlatyFight series featuring my mascot, the majestic Platymoose in some variety of street fighter get up.

**Medium:** Digital, Clip Studio Paint, Wacom MobileStudio Pro