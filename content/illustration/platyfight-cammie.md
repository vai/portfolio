---
date: 2017-03-14T01:46:42+02:00
description: ""
draft: false
featured: false
image: /images/illustration/platyfight-cammie-crop.jpg
share: false
slug: platyfight-cammie
tags:
- digital
- comic art
- platymoose
- street fighter
title: Platyfight Cammie
---

![Cammie](/images/illustration/platyfight-cammie.jpg)

Part of my PlatyFight series featuring my mascot, the majestic Platymoose in some variety of street fighter get up.

CAMMEH!

**Medium:** Digital, Clip Studio Paint, Wacom MobileStudio Pro

