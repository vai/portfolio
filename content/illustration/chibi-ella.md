---
date: 2017-03-03T11:19:41+02:00
description: ""
draft: false
featured: false
image: /images/illustration/chibi-pug.jpg
share: false
slug: chibi-pug
tags:
- digital
- comic art
- chibi
- gift
- animals
title: Chibi Pug
---

![Chibi Pug](/images/illustration/chibi-pug.jpg)

*Medium:* Digital, Clip Studio Paint, Wacom Intuos3