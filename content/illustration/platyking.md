---
date: 2017-02-25T17:53:21+02:00
description: "I thought you were a princess!"
draft: false
featured: false
image: /images/illustration/platyking.jpg
share: false
slug: platyking
tags:
- digital
- comic art
- platymoose
- adventure time
- street fighter
title: Platyfight Ice king
---
![Platyfight Ice King](/images/illustration/platyking.jpg "I thought you were a princess!")

Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose.

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

Cross over with Adventure Time !
