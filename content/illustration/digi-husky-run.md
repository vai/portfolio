---
date: 2017-02-25T19:00:57+02:00
description: ""
draft: false
featured: false
image: /images/illustration/digi-husky-run.jpg
share: false
slug: digi-husky-run
tags:
- digital
- figure study
- animals
title: Huskies Running
---
![Huskies](/images/illustration/digi-husky-run.jpg)

*Medium:* Digital, Adobe Photoshop, Wacom Intuos3