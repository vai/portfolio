---
date: 2017-02-25T15:48:23+02:00
description: "standing animation coming soon to a browser near you!"
draft: false
featured: false
image: /images/illustration/platyfight.jpg
share: false
slug: platyfight
tags:
- digital
- comic art
- platymoose
- street fighter
title: Platymoose Fight! 
---
![Platymoose Fight!](/images/illustration/platyfight.jpg)
Part of my PlatyFight series, featuring my mascot, the majestic Platymoose, in the street fighter pose. The original.

**Medium:** Digital, Clip Studio Paint, Wacom Intuos 3

