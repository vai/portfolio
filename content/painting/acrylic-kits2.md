---
date: 2017-02-25T20:38:07+02:00
description: ""
draft: false
featured: true
image: /images/painting/acrylic-kits2.jpg
share: false
slug: acrylic-kits2
tags:
- traditional
- acrylic
- cat
- animals
title: Shadow cat
---
![Shadow Kitsune](/images/painting/acrylic-kits2.jpg)

Capturing the darker side of my cat (far easier than the other…)

**Medium:** Acrylic paint on board

**Size:** 21cm x 29cm - A4