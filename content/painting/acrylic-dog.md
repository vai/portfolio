---
date: 2017-02-25T20:38:00+02:00
description: ""
draft: false
featured: false
image: /images/painting/acrylic-dog.jpg
share: false
slug: acrylic-dog
tags:
- acrylic
- traditional
- portraiture
- animals
title: Basset Hound
---
![Basset Hound](/images/painting/acrylic-dog.jpg)

**Medium:** Acrylic paint on board

**Size:** 21cm x 29cm - A4
