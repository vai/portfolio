---
date: 2017-02-25T19:02:22+02:00
description: ""
draft: false
featured: yes
image: /images/painting/digi-kits.jpg
share: false
slug: digi-kits
tags:
- portraiture
- animals
- cat
title: Sun Fox
---
![Kitsune in the sun](/images/painting/digi-kits.jpg "why is he so beautiful")
My kitsune, sitting in the sun, being very photogenic.

![Cropped](/images/painting/digi-kits-crop.jpg)

**Medium:** Digital, Adobe Photoshop, Wacom Intuos3