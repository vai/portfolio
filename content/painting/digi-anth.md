---
date: 2017-02-25T19:01:58+02:00
description: "played by dear sir Anthony Hopkins"
draft: false
featured: false
image: /images/painting/digi-anth.jpg
share: false
slug: digi-anth
tags:
- digital
- portraiture
- tribute
title: Frederick Treves
---

![Frederick Treves](/images/painting/digi-anth.jpg "from The Elephant Man")

**Medium:** Digital, Adobe Photoshop, Wacom Intuos3

Inspired by watching the Elephant Man (one of my favorite movies) I couldn't resist capturing Anthony's face in this clip