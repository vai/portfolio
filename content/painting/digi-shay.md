---
date: 2017-02-25T19:01:51+02:00
description: ""
draft: false
featured: false
image: /images/painting/digi-shay.jpg
share: false
slug: digi-shay
tags:
- digital
- portraiture
- gift
title: Shayaan
---
![Shayaan](/images/painting/digi-shay.jpg "I can't remember if she liked flowers or not")

**Medium:** Digital, Adobe Photoshop, Wacom Intuos3

A gift for a special friend