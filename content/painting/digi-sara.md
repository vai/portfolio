---
date: 2017-02-25T19:02:13+02:00
description: ""
draft: false
featured: false
image: /images/painting/digi-sara.jpg
share: false
slug: digi-sara
tags:
- digital
- portraiture
- commission
title: Boucle d'or
---
![Sister and brother](/images/painting/digi-sara.jpg)

**Medium:** Digital, Adobe Photoshop, Wacom Intuos3

A baby brother was born and welcomed