---
date: 2017-02-27T17:26:00+02:00
description: ""
draft: false
featured: false
image: /images/painting/chalk-jeans.jpg
share: false
slug: chalk-jeans
tags:
- traditional
- chalk pastel
- still life
title: Art Still Life
---
![Artsy stil life](/images/painting/chalk-jeans.jpg)

## Matric Final P.1

My first piece in chalk pastel, a still life representing me. Jeans, art stuff and my coffee cup … What else do I need?

**Medium:** Chalk pastel on paper

**Size:**  42 x 59 cm - A2
