---
date: 2017-02-25T19:02:26+02:00
description: ""
draft: false
featured: true
image: /images/painting/digi-roux.jpg
share: false
slug: digi-roux
tags:
- digital
- portraiture
title: Cheveux de feu
---
![Cheveux de feu](/images/painting/digi-roux.jpg "Et ses cheveux étaient feu")
**Medium:** Digital, Adobe Photoshop, Wacom Intuos3

Exploring faces and textures.

## The Work in progress

![WIP](/images/painting/digi-roux-wip2.jpg "Cheveux de feux en cours")

