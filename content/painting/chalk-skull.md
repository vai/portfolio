---
date: 2017-02-27T17:26:14+02:00
description: ""
draft: false
featured: false
image: /images/painting/chalk-skull.jpg
share: false
slug: chalk-skull
tags:
- traditional
- chalk pastel
- still life
- skull
title: Skull Life
---

![Skull Life](/images/painting/chalk-skull.jpg "and after only two lessons. ")

## Matric Final P.2

My second piece in chalk pastel, a more sombre still life. That wine glass was my nemesis.

**Medium:** Chalk pastel on paper

**Size:**  42 x 59 cm - A2