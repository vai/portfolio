---
date: 2017-02-25T20:38:09+02:00
description: ""
draft: false
featured: false
image: /images/painting/acrylic-kits1.jpg
share: false
slug: acrylic-kits1
tags:
- acrylic
- traditional
- portraiture
- cat
- animals
title: Angelic
---
![Angelic Kitsune](/images/painting/acrylic-kits1.jpg)

Capturing the lighter side to my cat

**Medium:** Acrylic paint on board

**Size:** 21cm x 29cm - A4