---
date: 2017-03-03T22:26:08+02:00
description: ""
draft: false
featured: false
image: /images/craft/bowl-koi.jpg
share: false
slug: bowl-koi
tags:
- acrylic
- glass
- hand painted
- animals
- koi fish
title: Koi Bowl
---

![Koi Bowl](/images/craft/bowl-koi.jpg)

Medium: A glass bowl, painted with acrylic paints and varnished with resin
