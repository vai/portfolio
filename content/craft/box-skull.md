---
date: 2017-02-26T21:13:09+02:00
description: ""
draft: false
featured: false
image: /images/craft/box-skull.jpg
share: false
slug: box-skull
tags:
- acrylic
- box
- hand painted
- sugar skull
- gift
title: Sugar Skull Box
---
Merry Día de Muertos! Enjoy the festive llama season. More accidentally offensive cultural reappropriation!  

## *Cosas Muertas* - Dead things

![Sugar Skull Box](/images/craft/box-skull.jpg)

## *Más dulce que la muerte* - Sweeter than death

![Sugar Skull Side](/images/craft/box-skull-dulce.jpg)

## *Cráneos y dulces* - skulls and candy

![Sugar Skull Side 2](/images/craft/box-skull-craneos.jpg)

Day of the dead inspired box of holding. Sugar skulls and spanish were the requirements.

**Medium**: A Galaxy phone box spray painted black, then hand painted in acrylic.

**Where it went**: A gift that is now holding macabre trinkets on a shelf somewhere, for someone wonderfully odd.

Another piece that I got to inject some other language in to. Yay