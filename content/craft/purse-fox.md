---
date: 2017-02-28T18:41:50+02:00
description: ""
draft: false
featured: false
image: /images/craft/purse-fox.jpg
share: false
slug: purse-fox
tags:
- felt
- handmade
- bag
- animals
- fox
title: Fox Purse
---

![Fox Purse](/images/craft/purse-fox.jpg)

**Medium:** Hand stitched felt purse with a sewn fabric inner lining. Magnets sewn into the top hem to close it.

**Size:** 11cm x 11cm - large enough to hold earphones, small enough to hide in your bag.

**Where it went:** A loving home where it protects earphones and lip ice.