---
date: 2017-02-25T21:05:29+02:00
description: ""
draft: false
featured: false
image: /images/craft/card-rick-morty.jpg
share: false
slug: rick-morty
tags:
- watercolor
- card
- hand painted
- rick and morty
title: Rick and Morty
---

![Rick and Morty](/images/craft/card-rick-morty.jpg "urrrrrrp")

**Medium:** Watercolor, pen and ink on card.

**Where it went:** My S.O's desk, reminding him to stay schwifty.