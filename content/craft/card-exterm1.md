---
date: 2017-02-25T21:04:09+02:00
description: ""
draft: false
featured: false
image: /images/craft/card-exterm1.jpg
share: false
slug: card-exterm
tags:
- watercolor
- card
- hand painted
- dr who
title: EXTERMINATE
---

![Dalek - exterminate](/images/craft/card-exterm1.jpg)
![Exterminate Inside](/images/craft/card-exterm2.jpg)

**Medium**: Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces. Including fanart stuffs. 
This was one of my favooorites