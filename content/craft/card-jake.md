---
date: 2017-02-25T21:05:20+02:00
description: ""
draft: false
featured: false
image: /images/craft/card-jake.jpg
share: false
slug: jake
tags:
- watercolor
- card
- hand painted
- adventure time
title: a Jake Hug
---

![Jake and crew](/images/craft/card-jake.jpg)

**Medium:** Watercolor and pen on card.

**Where it went:** Hanging on my wall, sending me encouragement through adventure time awesomeness.