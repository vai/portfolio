---
date: 2017-02-26T20:46:01+02:00
description: ""
draft: false
featured: false
image: /images/craft/card-tardis.jpg
share: false
slug: tardis
tags:
- watercolor
- pen and ink
- card
- hand painted
- dr who
title: Timey Wimey
---

![Tardis](/images/craft/card-tardis.jpg)

**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces. This is the one that got away, and is still lying in my drawer waiting for an owner!
Who wants a tardis in the mail?

