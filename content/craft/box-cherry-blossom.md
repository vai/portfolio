---
date: 2017-02-28T22:34:32+02:00
description: ""
draft: false
featured: true
image: /images/craft/box-cherry-blossom.jpg
share: false
slug: box-cherry-blossom
tags:
- acrylic
- box
- hand painted
- animals
- cherry blossom
- japanese
- gift
title: Cherry Blossom 
---
![Cherry Blossom Box](/images/craft/box-cherry-blossom.jpg)

## *tanjoubi omedetou* - happy birthday
![Cherry Blossom Birthday](/images/craft/box-cherry-birthday.jpg)

## *takusan no ai* - lots of love
![Cherry Blossom Love](/images/craft/box-cherry-love.jpg)

**Medium**: A Galaxy phone box spray painted white, then painted in Acrylic. 

**Where it went**: A gift for someones birthday. It now resides on a desk holding keepsakes for an equally elegant owner.

I really enjoyed this. The cherry blossoms were fun, the birds cute to paint and I got to write in japanese (:
