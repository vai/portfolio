---
date: 2017-02-25T21:04:08+02:00
description: "A wild Tardis appears!"
draft: false
featured: false
image: /images/craft/card-dr-who.jpg
share: false
slug: dr-who
tags:
- watercolor
- card
- hand painted
- dr who
title: Tardalek?
---

![A wild Tardis appears!](/images/craft/card-dr-who.jpg)


**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces, which this is one of. I had great fun designing whimsical cards that could surprise-cheer-up someones day.

