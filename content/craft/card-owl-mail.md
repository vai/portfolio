---
date: 2017-02-25T21:04:48+02:00
description: ""
draft: false
featured: false
image: /images/craft/card-owl-mail.jpg
share: false
slug: owl-mail
tags:
- pen and ink
- card
- hand painted
- harry potter
- owl
title: Owl Mail
---

![Owl Mail](/images/craft/card-owl-mail.jpg)

**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces. This is one of my favorites! Who doesn't want to get owl mail? 