---
date: 2017-02-28T22:34:18+02:00
description: ""
draft: false
featured: false
image: /images/craft/box-owl.jpg
share: false
slug: box-owl
tags:
- acrylic
- box
- hand painted
- animals
- owl
- french
title: La Chouette
---
## *Quelques chose* - Some things
![La chouette](/images/craft/box-owl.jpg "The owl")

## *La chouette et la lune* - the owl and the moon
![La Lune](/images/craft/box-owl-lune.jpg)

## *Sous les etoiles* - under the stars
![Les Etoiles](/images/craft/box-owl-etoiles.jpg)
---

**Medium**: A Galaxy phone box spray painted black and blue, then painted with acrylic.

**Where it went**: My shelf, holding my jewellery and whatever needs boxing.

This was the piece that got me started on the idea of repurposing old boxes and decorating them. 
I've been learning french, so I threw in some words that I enjoy, wrapped around my favorite bird of prey.