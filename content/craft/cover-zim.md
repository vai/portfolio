---
date: 2017-02-26T21:13:19+02:00
description: ""
draft: false
featured: false
image: /images/craft/cover-zim.jpg
share: false
slug: cover-zim
tags:
- acrylic
- hand painted
- phone cover
- invader zim
title: Zim Phone Cover
---

![Zim Phone Cover](/images/craft/cover-zim.jpg)

**Medium:** Acrylic paint on a Galaxy phone cover. Sprayed with matt varnish.

**Where it went:** My pocket! I'm obsessed with zim 