---
date: 2017-02-25T21:04:40+02:00
description: "yer a wizard"
draft: false
featured: false
image: /images/craft/card-hagrid.jpg
share: false
slug: hagrid
tags:
- pen and ink
- card
- hand painted
- harry potter
title: Hagrid
---

![Hagrid](/images/craft/card-hagrid.jpg)

**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces, which this is one of. I particularly liked the idea of sending a big, grizzly, safe, hagrid hug to someone. Totally consensual, of course.