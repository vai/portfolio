---
date: 2017-02-28T19:09:31+02:00
description: ""
draft: false
featured: true
image: /images/craft/box-black-bird.jpg
share: false
slug: box-black-bird
tags:
- acrylic
- box
- hand painted
- cherry blossom
- gift
title: Cherry Blossom Bird
---

![Cherry Blossoms](/images/craft/box-black-bird.jpg)

![The Back](/images/craft/box-black-2.jpg)

![The top](/images/craft/box-black-top.jpg)

**Medium**: A box of chocolates spray painted black, then hand painted in Acrylic.

**Where it went**: A gift for someone whose soul was born in the wrong country, and loves japan more than anything (even her pug.)

An enjoyable and tasty project, all in all. Om nom nom 




