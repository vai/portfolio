---
date: 2017-02-28T18:41:47+02:00
description: ""
draft: false
featured: true
image: /images/craft/purse-wolf.jpg
share: false
slug: purse-wolf
tags:
- felt
- handmade
- bag
- animals
- wolf
title: Wolf Purse
---

![Wolf Purse](/images/craft/purse-wolf.jpg)

**Medium:** Hand stitched felt and fur purse with a sewn fabric inner lining. Magnets sewn into the top hem to close it.

**Size:** 11cm x 11cm - large enough to hold earphones, small enough to hide in your bag.

**Where it went:** Travelling around in a bag, holding loose things and flashdrives that need protecting.

