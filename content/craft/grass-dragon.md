---
date: 2017-03-03T22:26:01+02:00
description: ""
draft: false
featured: false
image: /images/craft/grass-dragon.jpg
share: false
slug: grass-dragon
tags:
- acrylic
- sculpture
- hand painted
- fantasy art
- dragon
title: Grass Dragon
---

![Grass Dragon](/images/craft/grass-dragon.jpg)
![Grass Dragon Side](/images/craft/grass-dragon-2.jpg)

**Medium:** Fired pottery painted in acrylic. 

**Where it went:** It now lives in the garden, blending in with the thyme like a true mythical beast.