---
date: 2017-02-28T18:41:33+02:00
description: ""
draft: false
featured: false
image: /images/craft/bag-pencil-fox.jpg
share: false
slug: bag-pencil-fox
tags:
- felt
- handmade
- bag
- animals
- fox
title: Fox Pencil Bag
---

![Pencil Bag](/images/craft/bag-pencil-fox.jpg)

**Medium**: Hand stitched felt bag, with sewn inner lining. Magnets sewn into the nose flap to close it.

**Size**: 20cm x 15cm - large enough to hold a bunch of pencils.

**Where it went**: The original follows me around with my art supplies, like a cute bag of holding.

