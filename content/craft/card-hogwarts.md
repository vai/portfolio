---
date: 2017-02-25T21:04:35+02:00
description: ""
draft: false
featured: false
image: /images/craft/card-hogwarts.jpg
share: false
slug: hogwarts
tags:
- watercolor
- card
- hand painted
- harry potter
title: Hogwarts
---

![Hogwarts](/images/craft/card-hogwarts.jpg)

**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces, which this is one of. 
I had great fun designing whimsical cards that could surprise-cheer-up someones day. 