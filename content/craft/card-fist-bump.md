---
date: 2017-02-26T20:48:38+02:00
description: ""
draft: false
featured: false
image: /images/craft/card-fist-bump.jpg
share: false
slug: fist-bump
tags:
- watercolor
- card
- hand painted
- adventure time
title: Fist Bump!
---

![Finn and Jake](/images/craft/card-fist-bump.jpg)

**Medium:** Watercolor and pen on card.

**Where it went:** I took part in a card exchange group, which swiftly turned into sending each other art pieces, which this is one of. I had great fun designing whimsical cards that could surprise-cheer-up someones day.
