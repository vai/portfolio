---
date: 2017-02-27T18:16:26+02:00
description: ""
draft: false
featured: false
image: /images/sketch/sketch-closed.jpg
share: false
slug: closed
tags:
- doodle
- pen
- comic art
- vet life
title: A vet is never closed
---

![Closed!](/images/sketch/sketch-closed.jpg "there's always someone that needs something just as you're about to go home...")

